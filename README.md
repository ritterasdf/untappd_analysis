# What?
A couple of Python scripts to analyze beer rating data from Untappd. There will be more information in this readme at some point to explain the purpose of the scripts.

## Why?
Because why should data analytics companies be the only ones who use your Untappd data? There are a couple of questions these scripts will address:
1. How is your rating histogram split up by country?
2. How do your ratings compare to your friends and everyone else?
3. What are your preferences and how can you most efficiently find things you will like?

## Caveats
1. The first analysis script only works with a JSON-file downloaded from Untappd. To get a JSON file of your ratings, you have to pay a premium membership.
2. I have Untappd API access but will -for obvious reasons- not publish the keys here.
